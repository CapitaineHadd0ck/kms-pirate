# KMS Pirate

Environnement pour activer Windows 10 et Office 2019 sans pour autant faire confiance à des exécutables louches.

Les noms des scripts sont explicites, et le serveur KMS utilisé ([vlmcsd](https://github.com/kebe7jun/linux-kms-server)) est disponible sous license MIT.

Le port du protocole KMS est le TCP 1688, le serveur présent dans le script est le mien, je vais m'efforcer de le maintenir actif.


Inspiration : https://gist.github.com/CHEF-KOCH/1273041f0eafd20f2219