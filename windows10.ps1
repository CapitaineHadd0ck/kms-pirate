﻿<#
.NOTES
    Author: Hadd0ck
    Modified date: 2021-02-24
    Version 1.0
#>

$KMS = 'kms.IPv7.fr'

# https://py-kms.readthedocs.io/en/latest/Keys.html
$GVLKKeys = @(
    'TX9XD-98N7V-6WMQ6-BX7FG-H8Q99' # Windows 10 Home
    '3KHY7-WNT83-DGQKR-F7HPR-844BM' # Windows 10 Home N
    '7HNRX-D7KGG-3K4RQ-4WPJ4-YTDFH' # Windows 10 Home Single Language
    'PVMJN-6DFY6-9CCP6-7BKTT-D3WVR' # Windows 10 Home Country Specific
    'W269N-WFGWX-YVC9B-4J6C9-T83GX' # Windows 10 Professional
    'MH37W-N47XK-V7XM9-C7227-GCQG9' # Windows 10 Professional N
    'NW6C2-QMPVW-D7KKK-3GKT6-VCFB2' # Windows 10 Education
    '2WH4N-8QGBV-H22JP-CT43Q-MDWWJ' # Windows 10 Education N
    'NPPR9-FWDCX-D2C8J-H872K-2YT43' # Windows 10 Enterprise
    'DPH2V-TTNVB-4X9Q3-TJR4H-KHJW4' # Windows 10 Enterprise N
    'WNMTR-4C88C-JK8YV-HQ7T2-76DF9' # Windows 10 Enterprise 2015 LTSB
    '2F77B-TNFGY-69QQF-B8YKP-D69TJ' # Windows 10 Enterprise 2015 LTSB N
    'M7XTQ-FN8P6-TTKYV-9D4CC-J462D' # Windows 10 Enterprise LTSC 2019
)

# relaunch itself as admin if needed
if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
    Start-Process powershell.exe -ArgumentList "-File $PSCommandPath" -Verb runAs
    exit
}


Write-Host -ForegroundColor Cyan @"
*** Microsoft Windows 10 activation ***
    --- by Hadd0ck - 2021-02-24 ---

"@

Push-Location "$env:SystemRoot\System32"

Invoke-Expression 'cscript.exe slmgr.vbs /upk'$GVLKKeys | % { Invoke-Expression "cscript.exe slmgr.vbs /ipk $_" }Invoke-Expression "cscript.exe slmgr.vbs /skms $KMS"if (Invoke-Expression 'cscript.exe slmgr.vbs /ato | Select-String "Err"') {    Write-Host -ForegroundColor Red '*** Microsoft Windows 10 activation failed ***'
} else {
    Write-Host -ForegroundColor Green '*** Microsoft Windows 10 activation succeed ***'
}

Pop-Location
Pause
