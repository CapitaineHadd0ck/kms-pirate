﻿<#
.NOTES
    Author: Hadd0ck
    Modified date: 2021-02-24
    Version 2.0
#>

$KMS = 'kms.IPv7.fr'

# relaunch itself as admin if needed
if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
    Start-Process powershell.exe -ArgumentList "-File $PSCommandPath" -Verb runAs
    exit
}


function doTheThing {
    param (
        [Parameter(Mandatory)]
        [String]
        [ValidateScript({Test-Path "$_\Microsoft Office\Office16\OSPP.VBS"})]
        $InstallDir
    )
    
    Push-Location "$InstallDir\Microsoft Office\Office16"

    # activate Windows Script Host
    Set-ItemProperty 'HKCU:\SOFTWARE\Microsoft\Windows Script Host\Settings' -Name Enabled -Value 1
    # make sure .vbs file isn't associated with another program
    Invoke-Expression 'cmd /c assoc .vbs=VBSFile'

    Get-Item ..\root\Licenses16\ProPlusVL_* | % {
        Invoke-Expression "cscript.exe .\OSPP.VBS /inslic:'$_'"
    }

    Invoke-Expression 'cscript.exe slmgr.vbs /ckms'    Invoke-Expression 'cscript.exe .\OSPP.VBS /setprt:1688'    Invoke-Expression 'cscript.exe .\OSPP.VBS /unpkey:WFG99'    Invoke-Expression 'cscript.exe .\OSPP.VBS /unpkey:DRTFM'    Invoke-Expression 'cscript.exe .\OSPP.VBS /unpkey:BTDRB'    Invoke-Expression 'cscript.exe .\OSPP.VBS /inpkey:XQNVK-8JYDB-WJ9W3-YJ8YR-WFG99'

    Invoke-Expression "cscript.exe .\OSPP.VBS /sethst:$KMS"    if (Invoke-Expression 'cscript.exe .\OSPP.VBS /act | Select-String "ERROR"') {        Write-Host -ForegroundColor Red '*** Microsoft Office 365 activation failed ***'
    } else {
        Write-Host -ForegroundColor Green '*** Microsoft Office 365 activation succeed ***'
    }

    Pop-Location
    Pause
}


Write-Host -ForegroundColor Cyan @"
*** Microsoft Office 365 activation ***
    --- by Hadd0ck - 2021-02-24 ---

"@

$env:ProgramFiles, ${env:ProgramFiles(x86)} | % {
    if (Test-Path "$_\Microsoft Office\Office16\OSPP.VBS") {
        doTheThing $_
        break
    }
}
Write-Host -ForegroundColor Red 'OSPP.VBS not found'
Pause
